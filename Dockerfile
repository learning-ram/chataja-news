# build stage
FROM node:12 as build-stage
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY ./ .
RUN npm run build

# production stage
# FROM nginx:stable-alpine as production-stage
# COPY --from=build-stage /usr/src/app/dist /usr/share/nginx/html
# EXPOSE 8086
# CMD ["nginx", "-g", "daemon off;"]
FROM nginx as production-stage
RUN mkdir /app
COPY --from=build-stage /app/dist /app
COPY nginx.conf /etc/nginx/nginx.conf