// ALL ABOUT FUNCTION in HOMEPAGE
import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import moment from 'moment'
import corona from '@/store/corona'
import category from '@/store/category'
import latestNewsItem from '@/store/latestNewsItem'
import popularItems from '@/store/popularItems'
moment.locale('id')
const parseString = require('xml2js').parseString
Vue.use(Vuex)

var uzone = axios.get(process.env.VUE_APP_UZONE)
var kompasOlahraga = axios.get(process.env.VUE_APP_KOMPAS_OLAHRAGA)
var kompasNasional = axios.get(process.env.VUE_APP_KOMPAS_NASIONAL)

var latestNews = ['liputan6.com', 'kompas.com', 'okezone.com']
var recommendation = ['suara.com', 'liputan6.com', 'kompas.com', 'antaranews.com']
var headlineIndo = axios.get(`${process.env.VUE_APP_NEWS}/v2/top-headlines?country=id&apiKey=${process.env.VUE_APP_NEWS_KEY}`)
var latestNewsAPI = axios.get(`${process.env.VUE_APP_NEWS}/v2/everything?domains=${latestNews}&apiKey=${process.env.VUE_APP_NEWS_KEY}`)
var popularArticle = axios.get(`${process.env.VUE_APP_NEWS}/v2/everything?domains=liputan6.com,hipwee.com&apiKey=${process.env.VUE_APP_NEWS_KEY}`)
var recommendArticle = axios.get(`${process.env.VUE_APP_NEWS}/v2/everything?domains=${recommendation}&apiKey=${process.env.VUE_APP_NEWS_KEY}`)

export default new Vuex.Store({
  state: {
    headline: [],
    latestNews: [],
    recommendation: [],
    popular: {
      odd: [],
      even: []
    },
    searchData: [],
    searchTotal: 0,
    navbarMenu: []
  },
  // mutations: {
  //   SEARCH_DATA (state, data) {
  //     setTimeout(() => {
  //       state.searchData.total = data.length
  //     }, 30 * 100)
  //     // for (var i = 0; i < data.length; i++) {
  //     state.searchData.data = data
  //     // }
  //   }
  // },
  actions: {
    async getMenuCategory ({ state }) {
      state.navbarMenu = [
        {
          label: 'Berita Nasional',
          slug: 'berita-nasional'
        },
        {
          label: 'Olahraga',
          slug: 'olahraga'
        },
        {
          label: 'Sci-Tech',
          slug: 'sci-tech'
        },
        {
          label: 'Lifestyle',
          slug: 'lifestyle'
        },
        {
          label: 'Games',
          slug: 'games'
        },
        {
          label: 'Food',
          slug: 'food'
        },
        {
          label: 'Entertainment',
          slug: 'entertainment'
        },
        {
          label: 'Music',
          slug: 'music'
        }
      ]
    },
    async getHeadline ({ state }) {
      await headlineIndo.then((responses) => {
        // const newTitle = []
        for (var i = 0; i < 6; i++) {
          if (responses.data.articles[i].urlToImage === '') {
            responses.data.articles[i].urlToImage = require('../assets/logo.svg')
          }
          state.headline.push(responses.data.articles[i])
        }
      })
      // Random list when render in slider
      state.headline.sort(() => Math.random() - 0.5)
    },
    async getLatestNews ({ state }) {
      await latestNewsAPI.then((responses) => {
        for (let i = 0; i < 3; i++) {
          state.latestNews.push(responses.data.articles[i])
        }
      })
      state.latestNews.sort(() => Math.random() - 0.5)
    },
    async getPopArticle ({ state }) {
      await popularArticle.then((responses) => {
        for (let i = 0; i < 4; i++) {
          state.popular.odd.push(responses.data.articles[i])
        }
      })
    },
    async getRecommendation ({ state }) {
      await recommendArticle.then((responses) => {
        console.log(responses)
        for (let i = 0; i < responses.data.articles.length; i++) {
          state.recommendation.push(responses.data.articles[i])
        }
      })
      console.log(state.recommendation)
      // Shuffle index Array
      state.recommendation.sort(() => Math.random() - 0.5)
    },
    async searchNews ({ commit, state }, payload) {
      await kompasNasional.then((responses) => {
        parseString(responses.data, function (err, result) {
          if (err) throw err
          /* Specific News ex: COVID-19 */
          var hsl = result.rss.channel[0].item.filter((res) => {
            res.category_ca = 'Nasional'
            res.from = 'Kompas'
            res.img = res.thumb[0]
            return res.title[0].toLowerCase().indexOf(payload.search.toLowerCase()) >= 0
          })
          for (let i = 0; i < hsl.length; i++) {
            state.searchData.push(hsl[i])
          }
        })
      }).catch(err => {
        if (err.message === 'Network Error') {
          console.warn('There is a problem with the Kompas Nasional feed on Search News')
        }
      })
      await kompasOlahraga.then((responses) => {
        parseString(responses.data, function (err, result) {
          if (err) throw err
          var hsl = result.rss.channel[0].item.filter((res) => {
            res.category_ca = 'Nasional'
            res.from = 'Kompas'
            res.img = res.thumb[0]
            return res.title[0].toLowerCase().indexOf(payload.search.toLowerCase()) >= 0
          })
          for (let i = 0; i < hsl.length; i++) {
            state.searchData.push(hsl[i])
          }
        })
      }).catch(err => {
        if (err.message === 'Network Error') {
          console.warn('There is a problem with the Kompas Olahraga feed on Search News')
        }
      })
      await uzone.then((responses) => {
        parseString(responses.data, function (err, result) {
          if (err) throw err
          var hsl = result.rss.channel[0].item.filter((res) => {
            res.category_ca = 'Sci - Tech'
            res.from = 'Uzone'
            if (res.enclosure[0].$.url !== null || res.enclosure[0].$.url !== undefined || res.enclosure[0].$.url !== '') {
              res.img = res.enclosure[0].$.url
            } else {
              res.img = require('../assets/logo.svg')
            }
            return res.title[0].toLowerCase().indexOf(payload.search.toLowerCase()) >= 0
          })
          for (let i = 0; i < hsl.length; i++) {
            state.searchData.push(hsl[i])
          }
        })
      }).catch(err => {
        if (err.message === 'Network Error') {
          console.warn('There is a problem with the Uzone feed on Search News')
        }
      })

      setTimeout(() => {
        state.searchTotal = state.searchData.length
      }, 30 * 100)
    }
  },
  modules: {
    corona,
    category,
    latestNewsItem,
    popularItems
  }
})
