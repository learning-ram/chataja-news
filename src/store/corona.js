import axios from 'axios'
const CORS_PROXY = 'https://cors-anywhere.herokuapp.com/'
export default {
  state: {
    global: {
      positif: '',
      meninggal: '',
      sembuh: ''
    },
    indo: {
      positif: '',
      meninggal: '',
      sembuh: ''
    }
  },
  mutations: {},
  actions: {
    async getCoronaData ({ state }) {
      await axios.get(CORS_PROXY + 'https://api.kawalcorona.com/positif').then((res) => {
        state.global.positif = res.data.value
      })
      await axios.get(CORS_PROXY + 'https://api.kawalcorona.com/meninggal').then((res) => {
        state.global.meninggal = res.data.value
      })
      await axios.get(CORS_PROXY + 'https://api.kawalcorona.com/sembuh').then((res) => {
        state.global.sembuh = res.data.value
      })
    },
    async getIndoCorona ({ state }) {
      await axios.get(CORS_PROXY + 'https://api.kawalcorona.com/indonesia').then((res) => {
        state.indo.positif = res.data[0].positif
        state.indo.meninggal = res.data[0].meninggal
        state.indo.sembuh = res.data[0].sembuh
      })
    }
  },
  modules: {}
}
