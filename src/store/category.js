import axios from 'axios'
const parseString = require('xml2js').parseString

var uzone = axios.get(process.env.VUE_APP_UZONE)
var hipwee = axios.get(process.env.VUE_APP_HIPWEE)
var kompasOlahraga = axios.get(process.env.VUE_APP_KOMPAS_OLAHRAGA)
var kompasNasional = axios.get(process.env.VUE_APP_KOMPAS_NASIONAL)
var indoEsports = axios.get(process.env.VUE_APP_INDOESPORT)
export default {
  state: {
    beritaNasional: [],
    beritaOlahraga: [],
    beritaSciTech: [],
    beritaLifestyle: [],
    beritaGames: []
  },
  mutations: {},
  actions: {
    async getBeritaNasional ({ state }) {
      await kompasNasional.then((responses) => {
        parseString(responses.data, function (err, result) {
          if (err) throw err
          for (let i = 0; i < result.rss.channel[0].item.length; i++) {
          // for (let i = 0; i < 10; i++) {
            result.rss.channel[0].item[i].category_ca = 'Nasional'
            result.rss.channel[0].item[i].from = 'Kompas'
            result.rss.channel[0].item[i].img = result.rss.channel[0].item[i].thumb[0]
            state.beritaNasional.push(result.rss.channel[0].item[i])
          }
        })
      }).catch(err => {
        if (err.message === 'Network Error') {
          console.warn('There is a problem with the Kompas Nasional feed on Berita Nasional Category')
        }
      })
    },
    async getBeritaOlahraga ({ state }) {
      await kompasOlahraga.then((responses) => {
        parseString(responses.data, function (err, result) {
          if (err) throw err
          for (let i = 0; i < result.rss.channel[0].item.length; i++) {
            result.rss.channel[0].item[i].category_ca = 'Olahraga'
            result.rss.channel[0].item[i].from = 'Kompas'
            result.rss.channel[0].item[i].img = result.rss.channel[0].item[i].thumb[0]
            state.beritaOlahraga.push(result.rss.channel[0].item[i])
          }
        })
      }).catch(err => {
        if (err.message === 'Network Error') {
          console.warn('There is a problem with the Kompas Olahraga feed on Olahraga Category')
        }
      })
    },
    async getBeritaSciTech ({ state }) {
      await uzone.then((responses) => {
        parseString(responses.data, function (err, result) {
          if (err) throw err
          for (let i = 1; i < result.rss.channel[0].item.length; i++) {
            result.rss.channel[0].item[i].category_ca = 'Sci - Tech'
            result.rss.channel[0].item[i].from = 'Uzone'
            if (result.rss.channel[0].item[i].enclosure[0].$.url !== null || result.rss.channel[0].item[i].enclosure[0].$.url !== undefined || result.rss.channel[0].item[i].enclosure[0].$.url !== '') {
              result.rss.channel[0].item[i].img = result.rss.channel[0].item[i].enclosure[0].$.url
            } else {
              result.rss.channel[0].item[i].img = require('../assets/logo.svg')
            }
            state.beritaSciTech.push(result.rss.channel[0].item[i])
          }
        })
      }).catch(err => {
        if (err.message === 'Network Error') {
          console.warn('There is a problem with the Uzone feed on Sci Tech Category')
        }
      })
    },
    async getBeritaLifestyle ({ state }) {
      await hipwee.then((responses) => {
        parseString(responses.data, function (err, result) {
          if (!err) {
            for (let i = 0; i < result.rss.channel[0].item.length; i++) {
              result.rss.channel[0].item[i].category_ca = 'Lifestyle'
              result.rss.channel[0].item[i].from = 'Hipwee'
              var imgHipwee = result.rss.channel[0].item[i].description
              var getImgEven = /height="203" src="\s*(.*?)\s*" class/g
              var even = getImgEven.exec(imgHipwee)
              if (even !== null) {
                result.rss.channel[0].item[i].img = even[1]
              } else {
                result.rss.channel[0].item[i].img = require('../assets/logo.svg')
              }
              state.beritaLifestyle.push(result.rss.channel[0].item[i])
            }
          }
        })
      }).catch(err => {
        if (err.message === 'Network Error') {
          console.warn('There is a problem with the Hipwee feed on Lifestyle Category')
        }
      })
    },
    async getBeritaGames ({ state }) {
      await indoEsports.then((responses) => {
        parseString(responses.data, function (err, result) {
          if (err) throw err
          console.log(result.rss)
          for (var i = 0; i < result.rss.channel[0].item.length; i++) {
            result.rss.channel[0].item[i].category_ca = 'Games'
            result.rss.channel[0].item[i].from = 'indoesports'
            if (result.rss.channel[0].item[i].enclosure[0].$.url !== null || result.rss.channel[0].item[i].enclosure[0].$.url !== undefined || result.rss.channel[0].item[i].enclosure[0].$.url !== '') {
              result.rss.channel[0].item[i].img = result.rss.channel[0].item[i].enclosure[0].$.url
            } else {
              result.rss.channel[0].item[i].img = require('../assets/logo.svg')
            }
            state.beritaGames.push(result.rss.channel[0].item[i])
          }
        })
      }).catch(err => {
        if (err.message === 'Network Error') {
          console.warn('There is a problem with the Indoesports feed on Games Category')
        }
      })
    }
    // async getBeritaNasional ({ state }) {
    //   await kompasNasional.then((responses) => {
    //     parseString(responses.data, function (err, result) {
    //       if (err) throw err
    //       for (let i = 0; i < result.rss.channel[0].item.length; i++) {
    //       // for (let i = 0; i < 10; i++) {
    //         result.rss.channel[0].item[i].category_ca = 'Nasional'
    //         result.rss.channel[0].item[i].from = 'Kompas'
    //         if (i % 2) {
    //           result.rss.channel[0].item[i].img = result.rss.channel[0].item[i].thumb[0]
    //           state.beritaNasional.even.push(result.rss.channel[0].item[i])
    //         } else {
    //           result.rss.channel[0].item[i].img = result.rss.channel[0].item[i].thumb[0]
    //           state.beritaNasional.odd.push(result.rss.channel[0].item[i])
    //         }
    //       }
    //     })
    //   }).catch(err => {
    //     if (err.message === 'Network Error') {
    //       console.warn('There is a problem with the Kompas Nasional feed on Berita Nasional Category')
    //     }
    //   })
    // },
  },
  modules: {}
}
