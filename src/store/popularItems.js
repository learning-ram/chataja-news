import axios from 'axios'
const parseString = require('xml2js').parseString

var uzone = axios.get(process.env.VUE_APP_UZONE)
var hipwee = axios.get(process.env.VUE_APP_HIPWEE)
var kompasOlahraga = axios.get(process.env.VUE_APP_KOMPAS_OLAHRAGA)
var kompasNasional = axios.get(process.env.VUE_APP_KOMPAS_NASIONAL)
var indoEsports = axios.get(process.env.VUE_APP_INDOESPORT)

export default {
  state: {
    popular: {
      odd: [],
      even: []
    }
  },
  mutations: {},
  actions: {
    async getPopularItem ({ state }) {
      await uzone.then((responses) => {
        parseString(responses.data, function (err, result) {
          if (err) throw err
          for (let i = 5; i < result.rss.channel[0].item.length; i++) {
            result.rss.channel[0].item[i].category_ca = 'Sci - Tech'
            result.rss.channel[0].item[i].from = 'Uzone'
            if (i % 2) {
              if (result.rss.channel[0].item[i].enclosure[0].$.url !== null || result.rss.channel[0].item[i].enclosure[0].$.url !== undefined || result.rss.channel[0].item[i].enclosure[0].$.url !== '') {
                result.rss.channel[0].item[i].img = result.rss.channel[0].item[i].enclosure[0].$.url
              } else {
                result.rss.channel[0].item[i].img = require('../assets/logo.svg')
              }
              state.popular.even.push(result.rss.channel[0].item[i])
            } else {
              if (result.rss.channel[0].item[i].enclosure[0].$.url !== null || result.rss.channel[0].item[i].enclosure[0].$.url !== undefined || result.rss.channel[0].item[i].enclosure[0].$.url !== '') {
                result.rss.channel[0].item[i].img = result.rss.channel[0].item[i].enclosure[0].$.url
              } else {
                result.rss.channel[0].item[i].img = require('../assets/logo.svg')
              }
              state.popular.odd.push(result.rss.channel[0].item[i])
            }
          }
        })
      }).catch(err => {
        if (err.message === 'Network Error') {
          console.warn('There is a problem with the Uzone feed on More Berita Populer')
        }
      })
      await hipwee.then((responses) => {
        parseString(responses.data, function (err, result) {
          if (!err) {
            for (let i = 5; i < result.rss.channel[0].item.length; i++) {
              result.rss.channel[0].item[i].category_ca = 'Lifestyle'
              result.rss.channel[0].item[i].from = 'Hipwee'
              var imgHipwee = result.rss.channel[0].item[i].description
              if (i % 2) {
                var getImgEven = /height="203" src="\s*(.*?)\s*" class/g
                var even = getImgEven.exec(imgHipwee)
                if (even !== null) {
                  result.rss.channel[0].item[i].img = even[1]
                } else {
                  result.rss.channel[0].item[i].img = require('../assets/logo.svg')
                }
                state.popular.even.push(result.rss.channel[0].item[i])
              } else {
                var getImgOdd = /height="203" src="\s*(.*?)\s*" class/g
                var odd = getImgOdd.exec(imgHipwee)
                if (odd !== null) {
                  result.rss.channel[0].item[i].img = odd[1]
                } else {
                  result.rss.channel[0].item[i].img = require('../assets/logo.svg')
                }
                state.popular.odd.push(result.rss.channel[0].item[i])
              }
            }
          }
        })
      }).catch(err => {
        if (err.message === 'Network Error') {
          console.warn('There is a problem with the Hipwee feed on More Berita Populer')
        }
      })
      await kompasOlahraga.then((responses) => {
        parseString(responses.data, function (err, result) {
          if (err) throw err
          for (let i = 5; i < result.rss.channel[0].item.length; i++) {
            result.rss.channel[0].item[i].category_ca = 'Olahraga'
            result.rss.channel[0].item[i].from = 'Kompas'
            if (i % 2) {
              result.rss.channel[0].item[i].img = result.rss.channel[0].item[i].thumb[0]
              state.popular.even.push(result.rss.channel[0].item[i])
            } else {
              result.rss.channel[0].item[i].img = result.rss.channel[0].item[i].thumb[0]
              state.popular.odd.push(result.rss.channel[0].item[i])
            }
          }
        })
      }).catch(err => {
        if (err.message === 'Network Error') {
          console.warn('There is a problem with the Kompas Olahraga feed on More Berita Populer')
        }
      })
      await kompasNasional.then((responses) => {
        parseString(responses.data, function (err, result) {
          if (err) throw err
          for (let i = 5; i < result.rss.channel[0].item.length; i++) {
            result.rss.channel[0].item[i].category_ca = 'Olahraga'
            result.rss.channel[0].item[i].from = 'Kompas'
            if (i % 2) {
              result.rss.channel[0].item[i].img = result.rss.channel[0].item[i].thumb[0]
              state.popular.even.push(result.rss.channel[0].item[i])
            } else {
              result.rss.channel[0].item[i].img = result.rss.channel[0].item[i].thumb[0]
              state.popular.odd.push(result.rss.channel[0].item[i])
            }
          }
        })
      }).catch(err => {
        if (err.message === 'Network Error') {
          console.warn('There is a problem with the Kompas Olahraga feed on More Berita Populer')
        }
      })
      await indoEsports.then((responses) => {
        parseString(responses.data, function (err, result) {
          if (err) throw err
          console.log(result.rss)
          for (var i = 5; i < result.rss.channel[0].item.length; i++) {
            result.rss.channel[0].item[i].category_ca = 'Games'
            result.rss.channel[0].item[i].from = 'indoesports'
            if (i % 2) {
              if (result.rss.channel[0].item[i].enclosure[0].$.url !== null || result.rss.channel[0].item[i].enclosure[0].$.url !== undefined || result.rss.channel[0].item[i].enclosure[0].$.url !== '') {
                result.rss.channel[0].item[i].img = result.rss.channel[0].item[i].enclosure[0].$.url
              } else {
                result.rss.channel[0].item[i].img = require('../assets/logo.svg')
              }
              state.popular.even.push(result.rss.channel[0].item[i])
            } else {
              if (result.rss.channel[0].item[i].enclosure[0].$.url !== null || result.rss.channel[0].item[i].enclosure[0].$.url !== undefined || result.rss.channel[0].item[i].enclosure[0].$.url !== '') {
                result.rss.channel[0].item[i].img = result.rss.channel[0].item[i].enclosure[0].$.url
              } else {
                result.rss.channel[0].item[i].img = require('../assets/logo.svg')
              }
              state.popular.odd.push(result.rss.channel[0].item[i])
            }
          }
        })
      }).catch(err => {
        if (err.message === 'Network Error') {
          console.warn('There is a problem with the Indoesports feed on More Berita Populer')
        }
      })
      state.popular.odd.sort(() => Math.random() - 0.5)
      state.popular.even.sort(() => Math.random() - 0.5)
    }
  },
  modules: {}
}
