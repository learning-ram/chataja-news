import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
import moment from 'moment'
import momentTime from 'moment-timezone'
import VueMeta from 'vue-meta'

momentTime.tz.setDefault('Asia/Jakarta')
Vue.use(Buefy)
Vue.config.productionTip = false
Vue.use(VueMeta, {
  // optional pluginOptions
  refreshOnceOnNavigation: true
})
Vue.filter('formatDate', (value) => {
  if (value) {
    return moment(String(value)).format('DD/MM/YYYY')
  }
})

require('./assets/scss/main.scss')
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
