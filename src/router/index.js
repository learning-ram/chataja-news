import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import latestNews from '../views/latestNews.vue'
import Popular from '../views/Popular.vue'
import category from '../views/Category.vue'

Vue.use(VueRouter)

export default new VueRouter({
  mode: 'history',
  routes: [{
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/berita-terkini',
    name: 'LatestNews',
    component: latestNews
  },
  {
    path: '/berita-populer',
    name: 'Popular',
    component: Popular
  },
  {
    path: '/category/:slug',
    name: 'Category',
    component: category
  },
  {
    path: '/search',
    name: 'Search',
    component: category
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
  ]
})
