module.exports = {
  preset: '@vue/cli-plugin-unit-jest',
  // specifies aliases for imports
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/src/$1',
    '^~/(.*)$': '<rootDir>/$1',
    '^vue$': 'vue/dist/vue.common.js',
    "\\.(css|less|scss|sss|styl)$": "<rootDir>/node_modules/jest-css-modules",
    "\\.(css|less|scss|sss|styl)$": "<rootDir>/node_modules/buefy",
  },
  // specifies extensions of files that will be tested
  moduleFileExtensions: ['js', 'vue', 'json', 'ts'],
  // specifies a regular extension that will be used to find test files in your project
  // testRegex: '(/__tests__/.*|\\.(test|spec))\\.(js)$',
  // specifies that .js files must be transformed using babel-jest, and vue files must be transformed using vue-jest
  transform: {
    '^.+\\.js$': 'babel-jest',
    '.*\\.(vue)$': 'vue-jest'
  },
  setupFiles:["<rootDir>/browserMocks.js"],
  transformIgnorePatterns: ['/node_modules/'],
}
