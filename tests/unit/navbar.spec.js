import { shallowMount, createLocalVue, RouterLinkStub   } from '@vue/test-utils'
import Navbar from '@/components/Navbar.vue'
import VueRouter from 'vue-router'
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'

const localVue = createLocalVue()
// localVue.use(VueRouter)
// const router = new VueRouter()
localVue.use(Buefy)

// Mocking Local Storage
var localStorageMock = (function() {
  var store = [];
  return {
    getItem: function(key) {
      return store[key];
    },
    setItem: function(key, value) {
      store[key] = value;
    }
  }
})();
Object.defineProperty(window, 'localStorage', { value: localStorageMock })

// Mocking JSON parse
JSON.parse = jest.fn()

describe('Navbar.vue', () => {
  it('clickOutside Directives', () => {  
    const $route = {
      query: {
        key: 'asd'
      },
      params: {
        slug: 'berita-terkini'
      }
    }
    
    const wrapper = shallowMount(Navbar, {
      localVue,
      stubs: {
        RouterLink: RouterLinkStub
      },
      mocks: {
        $route
      }
    })
    wrapper.vm.historySearch = ['asd']
    wrapper.vm.historyPanel = true
    expect(wrapper.vm.$el.click()).toBe(undefined)
  })
  it('set focusSearch tobe true', () => {
    const $route = {
        query: {
          key: 'asd'
        },
        params: {
          slug: 'berita-terkini'
        }
      }
      
    const wrapper = shallowMount(Navbar, {
      localVue,
      mocks: {
        $route
      }
    })
    wrapper.setData({focusSearch: true})
    expect(wrapper.find('#inputSearch').exists()).toBe(true)
  })
  it('set focusSearch tobe false', () => {
    const $route = {
        query: {
          key: 'asd'
        },
        params: {
          slug: 'berita-terkini'
        }
      }
      
    const wrapper = shallowMount(Navbar, {
      localVue,
      mocks: {
        $route
      }
    })
    wrapper.vm.$options.watch.focusSearch.call(wrapper.vm, false);
    expect(wrapper.vm.focusSearch).toBe(false)
  })
  it('get item from localStorage', () => {
      const $route = {
        query: {
          key: 'asd'
        },
        params: {
          slug: 'berita-terkini'
        }
      }
      
    shallowMount(Navbar, {
      localVue,
      mocks: {
        $route
      }
    })
    // Set LocalStorage
    localStorage.setItem('ChatAjaNews_history',['asd'])
    expect(localStorage.getItem('ChatAjaNews_history')).toEqual(['asd'])
  })
  it('close focus', ()=>{
      const $route = {
        query: {
          key: 'asd'
        },
        params: {
          slug: 'berita-terkini'
        }
      }
      
    const wrapper = shallowMount(Navbar, {
      localVue,
      mocks: {
        $route
      }
    })
    wrapper.vm.closeFocus()
    expect(wrapper.vm.focusSearch).toBe(false)
  })
  it('hide History Panel', ()=>{
      const $route = {
        query: {
          key: 'asd'
        },
        params: {
          slug: 'berita-terkini'
        }
      }
      
    const wrapper = shallowMount(Navbar, {
      localVue,
      mocks: {
        $route
      }
    })
    wrapper.vm.hideHistoryPanel()
    expect(wrapper.vm.historyPanel ).toBe(false)
  })
  it('remove history of search', () => {
    const $route = {
      query: {
        key: 'asd'
      },
      params: {
        slug: 'berita-terkini'
      }
    }
    const wrapper = shallowMount(Navbar, {
      localVue,
      mocks: {
        $route
      }
    })
    wrapper.vm.clearHistory()
    expect(wrapper.vm.historySearch.length).toEqual(0)
  })
  it('remove history', () => {
    const $route = {
      query: {
        key: 'asd'
      },
      params: {
        slug: 'berita-terkini'
      }
    }
    const wrapper = shallowMount(Navbar, {
      localVue,
      mocks: {
        $route
      }
    })
    // Set item localstorage
    localStorage.setItem('ChatAjaNews_history',['asd'])
    // Get item localstorage
    JSON.parse = jest.fn().mockImplementationOnce(() => {
      return localStorage.getItem('ChatAjaNews_history')
    });
    wrapper.vm.historySearch = ['asd','bcd']
    wrapper.vm.removeHistory('asd')
    expect(wrapper.vm.historySearch.length).toEqual(1)
  })
  it('searching', () => {
    // Mocking $route
    localVue.use(VueRouter)
    const router = new VueRouter({ routes: [{ path: '/search', query: { key: 'Telkom' }}] })
    const wrapper = shallowMount(Navbar, { localVue, router })

    // act
    router.push({ path: '/search' })
    
    wrapper.vm.historySearch = ['asd','bcd']
    wrapper.vm.searching('pemilu')
    expect(wrapper.vm.historySearch.length).toEqual(3)
  })
})
